package com.bh.build.reg.citizensregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CitizensRegistryApplication {

	public static void main(String[] args) {

		SpringApplication.run(CitizensRegistryApplication.class, args);
	}

}
