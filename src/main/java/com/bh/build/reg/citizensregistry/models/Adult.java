package com.bh.build.reg.citizensregistry.models;

import lombok.Data;

import java.util.List;

@Data
public class Adult extends Person {

    List<Child> children;
}
