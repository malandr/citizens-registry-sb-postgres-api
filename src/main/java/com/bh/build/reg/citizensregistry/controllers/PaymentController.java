package com.bh.build.reg.citizensregistry.controllers;

import com.bh.build.reg.citizensregistry.models.Payment;
import com.bh.build.reg.citizensregistry.repositories.PaymentRepository;
import com.bh.build.reg.citizensregistry.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;

@Controller
public class PaymentController {

    private final PaymentRepository paymentRepository;
    private final PersonRepository personRepository;

    @Autowired
    public PaymentController(PaymentRepository paymentRepository,
                             PersonRepository personRepository) {
        this.paymentRepository = paymentRepository;
        this.personRepository = personRepository;
    }

    @GetMapping("/payments")
    public String getPayments(Model model) {

        model.addAttribute("payments", paymentRepository.findAll());

        return "payments";
    }

    @GetMapping("/payments/new")
    public String getForm(Model model) {

        model.addAttribute("payment", new Payment());

        return "create-payment";
    }

    @PostMapping("/payments/new")
    public String createPayment(@ModelAttribute Payment payment, Model model) {

        Payment newPayment = new Payment();

        newPayment.setPaymentDate(LocalDate.now());
        newPayment.setSumm(payment.getSumm());
        newPayment.setPerson(personRepository.findById(1L).get());

        paymentRepository.save(newPayment);

        return "payments";
    }
}
