package com.bh.build.reg.citizensregistry.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import java.util.List;

@Data
public class Child extends Person {

    private List<Adult> parents;
}
