package com.bh.build.reg.citizensregistry.repositories;

import com.bh.build.reg.citizensregistry.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {


}
