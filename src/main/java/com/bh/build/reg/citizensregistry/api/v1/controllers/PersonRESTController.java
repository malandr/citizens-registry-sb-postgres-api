package com.bh.build.reg.citizensregistry.api.v1.controllers;

import com.bh.build.reg.citizensregistry.models.Person;
import com.bh.build.reg.citizensregistry.repositories.PersonRepository;
import com.bh.build.reg.citizensregistry.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PersonRESTController.BASE_URL)
public class PersonRESTController {

    final static String BASE_URL = "/api/v1/persons";

    private final PersonService personService;
    private final PersonRepository personRepository;

    @Autowired
    public PersonRESTController(PersonService personService,
                                PersonRepository personRepository) {
        this.personService = personService;
        this.personRepository = personRepository;
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<Person> getAllPersons() {

        return personRepository.findAll();
    }

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Person getPerson(@PathVariable long id) {

        return personRepository.findById(id).get();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Person createPersons(@RequestBody Person person) {

        return personRepository.save(person);
    }
}
