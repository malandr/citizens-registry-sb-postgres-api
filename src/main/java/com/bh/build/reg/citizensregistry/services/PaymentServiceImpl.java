package com.bh.build.reg.citizensregistry.services;

import com.bh.build.reg.citizensregistry.models.Payment;
import com.bh.build.reg.citizensregistry.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService {

    private final PersonService personService;
    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentServiceImpl(PersonService personService,
                              PaymentRepository paymentRepository) {
        this.personService = personService;
        this.paymentRepository = paymentRepository;
    }

    @Override
    public List<Payment> findPaymentsBySumm(int summ) {

        List<Payment> payments = paymentRepository.findAll();
        List<Payment> foundPayments = new ArrayList<>();

        for (Payment payment : payments) {
            if (payment.getSumm() == summ) {
                foundPayments.add(payment);
            }
        }

        return foundPayments;

    }

    @Override
    public List<Payment> findPaymentsByPerson(String personLastName) {

        List<Payment> payments = paymentRepository.findAll();
        List<Payment> foundPayments = new ArrayList<>();

        for (Payment payment : payments) {
            if (payment.getPerson().getLastName().equals(personLastName)) {
                foundPayments.add(payment);
            }
        }

        return foundPayments;
    }

    @Override
    public List<Payment> findPaymentsByDate(String date) {

        List<Payment> payments = paymentRepository.findAll();
        List<Payment> foundPayments = new ArrayList<>();

//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        for (Payment payment : payments) {

            if (date.equals(payment.getPaymentDate().toString())) {
                foundPayments.add(payment);
            }
        }

        return foundPayments;

    }
}
