package com.bh.build.reg.citizensregistry.services;

import com.bh.build.reg.citizensregistry.models.Payment;
import com.bh.build.reg.citizensregistry.models.Person;
import com.bh.build.reg.citizensregistry.repositories.PersonRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public List<Person> findPersonsByFirstName(String firstName) {

        List<Person> personList = personRepository.findAll();
        List<Person> foundPersons = new ArrayList<>();

        for (Person person : personList) {
            if (person.getFirstName().equals(firstName)) {
                foundPersons.add(person);
            }
        }

        return foundPersons;
    }

    @Override
    public List<Person> findPersonsByLastName(String lastName) {

        List<Person> personList = personRepository.findAll();
        List<Person> foundPersons = new ArrayList<>();

        for (Person person : personList) {
            if (person.getFirstName().equals(lastName)) {
                foundPersons.add(person);
            }
        }

        return foundPersons;
    }

    @Override
    public Person findPersonById(Long id) {

        return personRepository.findById(id).get();
    }

    @Override
    public Person findPersonByPayment(Payment payment) {

        return payment.getPerson();
    }
}
