package com.bh.build.reg.citizensregistry.services;

import com.bh.build.reg.citizensregistry.models.Address;
import com.bh.build.reg.citizensregistry.repositories.AddressRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public List<Address> findAddressesByStreet(String streetName) {

        List<Address> addresses = addressRepository.findAll();
        List<Address> foundAddresses = new ArrayList<>();

        for (Address address : addresses) {
            if (address.getStreetName().equals(streetName)) {
                foundAddresses.add(address);
            }
        }

        return foundAddresses;
    }

    @Override
    public List<Address> findAddressesByBuildingNumber(int buildingNumber) {

        List<Address> addresses = addressRepository.findAll();
        List<Address> foundAddresses = new ArrayList<>();

        for (Address address : addresses) {
            if (address.getStreetBuilding() == buildingNumber) {
                foundAddresses.add(address);
            }
        }

        return foundAddresses;
    }

    @Override
    public List<Address> findAddressesByEntrywayNumber(int entrywayNumber) {

        List<Address> addresses = addressRepository.findAll();
        List<Address> foundAddresses = new ArrayList<>();

        for (Address address : addresses) {
            if (address.getEntryway() == entrywayNumber) {
                foundAddresses.add(address);
            }
        }

        return foundAddresses;
    }
}
