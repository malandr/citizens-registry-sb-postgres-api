package com.bh.build.reg.citizensregistry.api.v1.controllers;

import com.bh.build.reg.citizensregistry.models.Payment;
import com.bh.build.reg.citizensregistry.repositories.PaymentRepository;
import com.bh.build.reg.citizensregistry.services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(PaymentRESTController.BASE_URL)
public class PaymentRESTController {

    static final String BASE_URL = "/api/v1/payments";

    private final PaymentRepository paymentRepository;
    private final PaymentService paymentService;

    @Autowired
    public PaymentRESTController(PaymentRepository paymentRepository,
                                 PaymentService paymentService) {
        this.paymentRepository = paymentRepository;
        this.paymentService = paymentService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Payment> getPayments() {

        return paymentRepository.findAll();
    }

    @GetMapping("/date/{date}")
    @ResponseStatus(HttpStatus.OK)
    public List<Payment> getPaymentsByDate(@PathVariable String date) {

        return paymentService.findPaymentsByDate(date);
    }

    @GetMapping("/id/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Payment getPaymentById(@PathVariable long id) {

        return paymentRepository.findById(id).get();
    }
}
