package com.bh.build.reg.citizensregistry.repositories;

import com.bh.build.reg.citizensregistry.models.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
