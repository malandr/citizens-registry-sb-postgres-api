package com.bh.build.reg.citizensregistry.controllers;

import com.bh.build.reg.citizensregistry.models.Person;
import com.bh.build.reg.citizensregistry.repositories.PersonRepository;
import com.bh.build.reg.citizensregistry.services.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class PersonController {

    private final PersonService personService;
    private final PersonRepository personRepository;

    @Autowired
    public PersonController(PersonService personService, PersonRepository personRepository) {
        this.personService = personService;
        this.personRepository = personRepository;
    }

    @GetMapping("/persons")
    public String getPersons(Model model) {

        List persons = personRepository.findAll();

        model.addAttribute("persons", persons);

        return "persons";
    }

    @GetMapping("/person/{id}")
    public String getPerson(@PathVariable long id, Model model) {

        model.addAttribute("person", personRepository.findById(id).get());

        return "person";
    }

    @GetMapping("/persons/new")
    public String getForm(Model model) {

        model.addAttribute("person", new Person());

        return "create-person";
    }

    @PostMapping("/persons/new")
    public String createPerson(@ModelAttribute Person person, Model model) {

        Person newPerson = new Person();



        return "redirect:/persons";
    }
}
