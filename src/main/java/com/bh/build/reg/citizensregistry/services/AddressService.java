package com.bh.build.reg.citizensregistry.services;

import com.bh.build.reg.citizensregistry.models.Address;

import java.util.List;

public interface AddressService {

    List<Address> findAddressesByStreet(String streetName);
    List<Address> findAddressesByBuildingNumber(int buildingNumber);
    List<Address> findAddressesByEntrywayNumber(int entrywayNumber);

}
