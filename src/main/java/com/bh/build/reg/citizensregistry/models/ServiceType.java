package com.bh.build.reg.citizensregistry.models;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public enum ServiceType {

   ELECTRICITY, GAS, WATER

}
