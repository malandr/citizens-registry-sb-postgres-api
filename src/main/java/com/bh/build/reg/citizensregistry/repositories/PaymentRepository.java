package com.bh.build.reg.citizensregistry.repositories;

import com.bh.build.reg.citizensregistry.models.Payment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PaymentRepository extends JpaRepository<Payment, Long> {


}
