package com.bh.build.reg.citizensregistry.services;

import com.bh.build.reg.citizensregistry.models.Payment;
import com.bh.build.reg.citizensregistry.models.Person;

import java.util.List;

public interface PersonService {

    List<Person> findPersonsByFirstName(String firstName);
    List<Person> findPersonsByLastName(String lastName);
    Person findPersonById(Long id);
    Person findPersonByPayment(Payment payment);
}
