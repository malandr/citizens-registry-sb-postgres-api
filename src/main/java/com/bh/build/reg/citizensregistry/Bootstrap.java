package com.bh.build.reg.citizensregistry;

import com.bh.build.reg.citizensregistry.models.Address;
import com.bh.build.reg.citizensregistry.models.Payment;
import com.bh.build.reg.citizensregistry.models.Person;
import com.bh.build.reg.citizensregistry.models.ServiceType;
import com.bh.build.reg.citizensregistry.repositories.AddressRepository;
import com.bh.build.reg.citizensregistry.repositories.PaymentRepository;
import com.bh.build.reg.citizensregistry.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class Bootstrap implements CommandLineRunner {

    private final PersonRepository personRepository;
    private final AddressRepository addressRepository;
    private final PaymentRepository paymentRepository;

    @Autowired
    public Bootstrap(PersonRepository personRepository,
                     AddressRepository addressRepository,
                     PaymentRepository paymentRepository) {
        this.personRepository = personRepository;
        this.addressRepository = addressRepository;
        this.paymentRepository = paymentRepository;
    }

    private void loadData() {

        Address address1 = new Address();
        address1.setId(1L);
        address1.setEntryway(1);
        address1.setStreetName("Shevchenko street");
        address1.setStreetBuilding(21);
        address1.setFlatNumber(17);
        addressRepository.save(address1);

        Address address2 = new Address();
        address2.setId(2L);
        address2.setStreetName("Skovorody avenue");
        address2.setStreetBuilding(7);
        address2.setEntryway(2);
        address2.setFlatNumber(56);
        addressRepository.save(address2);

        Person person1 = new Person();
        person1.setId(1L);
        person1.setFirstName("Olga");
        person1.setPatronymic("Iegorivna");
        person1.setLastName("Vasylenko");
        person1.setAge(33);
        person1.setSex("F");
        person1.setAddress(new Address());
        person1.setAddress(address1);
        personRepository.save(person1);

        Person person2 = new Person();
        person2.setId(2L);
        person2.setFirstName("Vasyl");
        person2.setPatronymic("Vasyliiovych");
        person2.setLastName("Vasylenko");
        person2.setAge(35);
        person2.setSex("M");
        person2.setAddress(new Address());
        person2.setAddress(address1);
        personRepository.save(person2);

        Payment payment1 = new Payment();
        payment1.setId(1L);
        payment1.setPaymentDate(LocalDate.now());
        payment1.setPerson(person1);
        payment1.setSumm(325);
        payment1.setServiceType(ServiceType.ELECTRICITY);
        paymentRepository.save(payment1);

    }

    @Override
    public void run(String... args) throws Exception {
        loadData();
    }
}
