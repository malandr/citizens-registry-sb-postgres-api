package com.bh.build.reg.citizensregistry.services;

import com.bh.build.reg.citizensregistry.models.Payment;
import com.bh.build.reg.citizensregistry.models.Person;

import java.util.List;

public interface PaymentService {

    List<Payment> findPaymentsBySumm(int summ);
    List<Payment> findPaymentsByPerson(String personLastName);
    List<Payment> findPaymentsByDate(String date);

}
