package com.bh.build.reg.citizensregistry.api.v1.controllers;

import com.bh.build.reg.citizensregistry.models.Address;
import com.bh.build.reg.citizensregistry.repositories.AddressRepository;
import com.bh.build.reg.citizensregistry.services.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(AddressRESTController.BASE_URL)
public class AddressRESTController {

    final static String BASE_URL = "/api/v1/addresses";

    private final AddressService addressService;
    private final AddressRepository addressRepository;

    @Autowired
    public AddressRESTController(AddressService addressService, AddressRepository addressRepository) {
        this.addressService = addressService;
        this.addressRepository = addressRepository;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Address> getAllAddresses() {

        return addressRepository.findAll();
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Address getAddressById(@PathVariable long id) {

        return addressRepository.findById(id).get();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Address createAddress(@RequestBody Address address) {

        return addressRepository.save(address);
    }
}
